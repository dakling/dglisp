(in-package :dglisp)

(defstruct coordinate
  "A cartesian coordinate in R^n represented as a list of n values"
  x-list)

(defun coordinate (&rest components)
  "Convenient and safe constructor"
  (assert (numberp (car components))
          nil
          "Attempt to create coordinate from existing coordinate")
  (make-coordinate
   :x-list components))

(defun get-coordinate (coord direction)
  (nth direction (coordinate-x-list coord)))

(defun x-coordinate (coord)
  "Return x-coordinate"
  (get-coordinate coord 0))

(defun y-coordinate (coord)
  "Return y-coordinate"
  (get-coordinate coord 1))

(defun z-coordinate (coord)
  "Return z-coordinate"
  (get-coordinate coord 2))

(defun coordinate-dimension (coord)
  (length (coordinate-x-list coord)))

(defun coordinate-add (coord1 coord2)
  (assert (= (coordinate-dimension coord1) (coordinate-dimension coord2))
          nil
          "Dimension mismatch when trying to add ~a and ~a"
          coord1
          coord2)
  (apply #'coordinate
         (mapcar #'+
                 (coordinate-x-list coord1)
                 (coordinate-x-list coord2))))

;; Convenient functions to check normal vectors,
;; assumes that they are multiples of the unit vector
;; -> this impementation only makes sense for 1-d and rectangular 2-d meshes
(defun west-p (coord)
  (and (> 0 (x-coordinate coord))
       (or (not (y-coordinate coord)) (= 0 (y-coordinate coord)))))

(defun east-p (coord)
  (and (< 0 (x-coordinate coord))
       (or (not (y-coordinate coord)) (= 0 (y-coordinate coord)))))

(defun north-p (coord)
  (and (= 0 (x-coordinate coord))
       (< 0 (y-coordinate coord))))

(defun south-p (coord)
  (and (= 0 (x-coordinate coord))
       (> 0 (y-coordinate coord))))

(defun unit-coordinate (direction dimension &optional (coefficient 1))
  "Construct a coordinate representing a multiple of the unit vector in direction."
  (apply #'coordinate
         (mapcar
          (lambda (x)
            (* x coefficient))
          (unit-list direction dimension))))

(defun to-unit-coordinate (coordinate cell)
  (apply
   #'coordinate
   (mapcar
    (lambda (x direction)
      (- (* 2
            (/ (cell-delta cell direction))
            (- x (cell-min cell direction)))
         1))
    (coordinate-x-list coordinate)
    (range (coordinate-dimension coordinate)))))
