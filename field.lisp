(in-package :dglisp)

(defstruct field
  local-field-list)

(defun make-field-list-from-y (y grid degree)
  (let* ((cell-alist (grid-cell-alist grid))
         (dimension (grid-dimension grid))
         (number-of-dofs-per-cell-and-depvar (get-number-of-dofs-per-cell-and-depvar
                                              degree
                                              dimension))
         (number-of-cells (grid-number-of-cells grid))
         (number-of-depvars (/ (magicl:size y) number-of-dofs-per-cell-and-depvar number-of-cells))
         (number-of-dofs-per-cell (get-number-of-dofs-per-cell
                                   degree
                                   number-of-depvars
                                   dimension)))
    (assert (integerp number-of-depvars)
            nil
            "length of y (~a) does not match grid with ~a cells and dg-degree ~a"
            (magicl:size y)
            number-of-cells
            degree)
    (mapcar
     (lambda (depvar)
       (make-field
        :local-field-list
        (mapcar
         (lambda (cell)
           (make-local-field
            :cell
            (cdr cell)
            :solution-list
            (mapcar
             (lambda (dg-index)
               (magicl:tref
                y
                (get-global-index
                 (car cell)
                 dg-index
                 depvar
                 number-of-dofs-per-cell
                 number-of-dofs-per-cell-and-depvar)
                0))
             (range number-of-dofs-per-cell-and-depvar))))
         cell-alist)))
     (range number-of-depvars))))

(defun make-unit-field-list (j setup)
  (make-field-list-from-y (unit-vector j (setup-number-of-dofs setup))
                          (setup-grid setup)
                          (setup-dg-degree setup)))

(defun make-zeros-field-list (setup)
  (make-field-list-from-y (zeros-vector (setup-number-of-dofs setup))
                          (setup-grid setup)
                          (setup-dg-degree setup)))

;; (make-field-list-from-y (magicl:from-list (range 8) (list 8 1))
;;                         (make-rectangular-grid-automatically (list 4)
;;                                                              (list 0 1))
;;                         0)

;; (make-field-list-from-y (magicl:from-list (range 54) (list 54 1))
;;                         (make-rectangular-grid-automatically (list 3 3)
;;                                                              (list 0 1 0 1))
;;                         1)

;; (print (magicl:size (magicl:from-list (range 27) (list 27 1))))

(defun get-global-index (cell-index dg-index depvar-index number-of-dofs-per-cell number-of-dofs-per-cell-and-depvar)
  (+ dg-index
     (* cell-index
        number-of-dofs-per-cell)
     (* depvar-index number-of-dofs-per-cell-and-depvar)))

(defun field-number-of-dofs-per-cell-and-depvar (field)
  (local-field-number-of-dofs-per-cell-and-depvar field))

;; (make-field-list-from-y (unit-vector 27))

(defun field-grid (field)
  (let* ((cell-list (mapcar #'local-field-cell (field-local-field-list field)))
         (number-of-cells (length cell-list)))
    (make-grid
     :cell-alist
     (mapcar (lambda (index cell)
               (cons index cell))
             (range number-of-cells)
             cell-list))))

;; (field-grid (nth 0 (make-field-list-from-y (magicl:from-list (range 8) (list 8 1))
;;                                     (make-rectangular-grid-automatically (list 4)
;;                                                                          (list 0 1))
;;                                     1)))

(defun field-to-alist (field)
  "Turn field into association list to allow looking up values conveniently"
  (mapcar (lambda (local-field)
            (with-slots (cell)
                local-field
              (cons cell local-field)))
          (field-local-field-list field)))

;; (field-to-alist (nth 0 (make-field-list-from-y (magicl:from-list (range 8) (list 8 1))
;;                                     (make-rectangular-grid-automatically (list 4)
;;                                                                          (list 0 1))
;;                                     1)))

(defun get-local-field (cell-index field)
  "Find the local field (cell) based on its index"
  (cdr (assoc (get-cell cell-index (field-grid field))
              (field-to-alist field))))

(defun get-local-field-x (field &rest x)
  "Find the local field (cell) based on coordinates x"
  (let ((coord
          (if
           (and (= 1 (length x)) (coordinate-p (car x)))
           (car x)
           (apply #'coordinate x))))
    (cdr (assoc (get-cell-x coord (field-grid field))
               (field-to-alist field)))))

;; (get-local-field 0 (nth 0 (make-field-list-from-y (magicl:from-list (range 54) (list 54 1))
;;                                                   (make-rectangular-grid-automatically (list 3 3)
;;                                                                                        (list 0 1 0 1))
;;                                                   1)))

;; (local-field-solution-list (car (field-local-field-list (nth 0 (make-field-list-from-y (magicl:from-list (range 54) (list 54 1))
;;                                                                                    (make-rectangular-grid-automatically (list 3 3)
;;                                                                                                                         (list 0 1 0 1))
;;                                                                                    1)))))

(defun field-get-coefficient (cell-index dg-index field)
  (nth dg-index
       (local-field-solution-list
        (get-local-field
         cell-index
         field))))

(defun field-list-get-coefficient (cell-index dg-index depvar field-list)
  (field-get-coefficient cell-index dg-index (nth depvar field-list)))

(defun field-dimension (field)
  (local-field-dimension (car (field-local-field-list field))))

(defun field-list-dimension (field-list)
  (field-dimension (car field-list)))
