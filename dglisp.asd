;;;; dglisp.asd

(asdf:defsystem #:dglisp
  :description "A toy Discontinuous Galerkin solver"
  :author "Dario Klingenberg"
  :license  "GPL3"
  :version "0.0.1"
  :serial t
  :depends-on (#:alexandria
               #:trivia
               #:magicl
               #:py4cl
               #:cl-csv)
  :components ((:file "package")
               (:file "helpers")
               (:file "coordinate")
               (:file "cell")
               (:file "cell-1d")
               (:file "cell-2d")
               (:file "grid")
               (:file "local-field")
               (:file "field")
               (:file "power")
               (:file "monomial")
               (:file "polynomial")
               (:file "basis")
               (:file "setup")
               (:file "assembly")
               (:file "solution")
               (:file "plotting")
               (:file "examples/simple-ode")
               (:file "examples/two-dep-vars-example")
               (:file "examples/two-d-simple")
               (:file "examples/two-d")
               (:file "dglisp"))
  :in-order-to ((test-op (test-op dglisp-test))))

(asdf:defsystem dglisp/test
  :depends-on (:dglisp
               :prove)
  :defsystem-depends-on (:prove-asdf)
  :components
  ((:test-file "tests/tests"))
  :perform (test-op :after (op c)
                    (funcall (intern #.(string :run) :prove) c)))
