(in-package :dglisp)

(defun solve (setup)
  (let ((matrix (assemble-matrix setup))
        (affine (assemble-affine setup)))
    (make-field-list-from-y
     (magicl:@ (magicl:inv matrix) affine)
     (setup-grid setup)
     (setup-dg-degree setup))))
