(in-package :dglisp)

(defclass cell-1d (cell)
  ((x-min :initarg :x-min
          :accessor x-min)
   (x-max :initarg :x-max
          :accessor x-max)))

(defun make-cell-1d (&key x-min x-max)
  (make-instance 'cell-1d
                 :x-min x-min
                 :x-max x-max))

(defmethod print-object ((obj cell-1d) stream)
      (print-unreadable-object (obj stream :type t)
        (with-accessors ((x-min x-min)
                         (x-max x-max))
            obj
          (format stream "[~a, ~a];" x-min x-max))))

(defun cell-1d-get-size (cell)
  (- (x-coordinate (x-max cell))
     (x-coordinate (x-min cell))))

(defmethod cell-transform-from-unit-cell ((cell cell-1d) expression)
  (* expression (/ (cell-1d-get-size cell) 2.0)))

(defmethod cell-transform-from-unit-edge ((cell cell-1d) normal expression)
  "In 1d, cell edges are just points, therefore, no transformation is necessary"
  expression)

(defmethod cell-transform-to-unit-cell ((cell cell-1d) expression)
  (if (listp expression)
      (mapcar
       (lambda (expr) (cell-transform-to-unit-cell cell expr))
       expression)
      (funcall
       (cond
         ((polynomial-p expression)
          #'polynomial-multiply)
         (t #'*))
       expression
       (/ 2.0 (cell-1d-get-size cell)))))

(defmethod cell-dimension ((cell cell-1d))
  1)

(defmethod point-in-cell-p ((cell cell-1d) x)
  (and (<= (x-coordinate (x-min cell)) (x-coordinate x))
       (>= (x-coordinate (x-max cell)) (x-coordinate x))))

(defmethod unit-cell-normal-list ((cell cell-1d))
  (list (coordinate 1) (coordinate -1)))

(defmethod cell-x-min ((cell cell-1d))
  (x-coordinate (x-min cell)))

(defmethod cell-x-max ((cell cell-1d))
  (x-coordinate (x-max cell)))

(defmethod cell-coordinate ((cell cell-1d))
  (coordinate (cell-x-mid cell)))

(defmethod cell-delta ((cell cell-1d) direction)
  (- (cell-x-max cell) (cell-x-min cell)))

(defmethod cell-min ((cell cell-1d) direction)
  (cell-x-min cell))
