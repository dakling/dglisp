(in-package :dglisp)

;; only 1-d and rectangular 2-d grids are fully supported
(defstruct grid 
  cell-alist
  number-of-cells-x
  number-of-cells-y
  number-of-cells-z) ;TODO only makes sense for rectangular grids -> improve representation if necessary

(defun rectangular-2d-grid-to-global-index (index-x index-y number-of-cells-x)
  (+ index-x (* index-y number-of-cells-x)))

(defun rectangular-2d-grid-to-local-index (global-index number-of-cells-x)
  (let ((index-x (mod global-index number-of-cells-x))
        (index-y (floor global-index number-of-cells-x)))
    (list index-x index-y)))

;; Wrapper for constructor
(defun make-rectangular-grid-automatically (number-of-cell-list bound-list)
  (let* ((dimension (length number-of-cell-list)))
    (assert (= dimension (/ (length bound-list) 2))
            (number-of-cell-list bound-list)
            "number-of-cell-list does not match bound-list")
    (cond ((= 1 dimension)
           (let* ((x-min (car bound-list))
                  (x-max (cadr bound-list))
                  (number-of-cells (car number-of-cell-list))
                  (node-locations (mapcar (lambda (x)
                                            (coordinate
                                             (+ 0.0 x-min
                                                (* (- x-max x-min)
                                                   (/ x number-of-cells)))))
                                          (range (1+ number-of-cells)))))
             
             (make-grid
              :cell-alist
              (mapcar (lambda (index x-min x-max)
                        (cons index (make-cell-1d :x-min x-min
                                                  :x-max x-max)))

                      (range number-of-cells)
                      (butlast node-locations)
                      (cdr node-locations))
              :number-of-cells-x number-of-cells)))
          ((= 2 dimension)
           (let* ((number-of-cells-x (car number-of-cell-list))
                  (number-of-cells-y (cadr number-of-cell-list))
                  (x-min (car bound-list))
                  (x-max (cadr bound-list))
                  (y-min (caddr bound-list))
                  (y-max (cadddr bound-list))
                  (nodes-x (mapcar
                            (lambda (x) (+ x-min
                                      (* (- x-max x-min)
                                         (/ x number-of-cells-x))))
                            (range (1+ number-of-cells-x))))
                  (nodes-y (mapcar
                            (lambda (y) (+ y-min
                                      (* (- y-max y-min)
                                         (/ y number-of-cells-y))))
                            (range (1+ number-of-cells-y)))))
             (make-grid
              :cell-alist
              (flatten-alist
               (mapcar
                (lambda (index-y y-min y-max)
                  (mapcar
                   (lambda (index-x x-min x-max)
                     (let ((index (rectangular-2d-grid-to-global-index
                                   index-x
                                   index-y
                                   number-of-cells-x)))
                       (cons index (make-cell-2d-rectangle
                                    :x-min x-min
                                    :x-max x-max
                                    :y-min y-min
                                    :y-max y-max))))
                   (range number-of-cells-x)
                   (butlast nodes-x)
                   (cdr nodes-x)))
                (range number-of-cells-y)
                (butlast nodes-y)
                (cdr nodes-y)))
              :number-of-cells-x number-of-cells-x
              :number-of-cells-y number-of-cells-y))))))

(defun grid-number-of-cells (grid)
  (length (grid-cell-alist grid)))

(defun get-cell (index grid)
  (cdr (assoc index (grid-cell-alist grid))))

(defun get-cell-index (cell grid)
  (car (rassoc cell (grid-cell-alist grid))))

(defun get-cell-x (x grid)
  (cdr (rassoc-if (lambda (cell)
                    (point-in-cell-p cell x))
                  (grid-cell-alist grid))))

(defun to-local-coordinate (x grid)
  (let* ((cell (get-cell-x x grid))
         (x-min (cell-x-min cell)))
   (cond 
     ((= 1 (grid-dimension grid))
      (let ((transformation-factor (cell-transform-to-unit-cell cell 1)))
        (- (* (- (x-coordinate x) x-min)
              transformation-factor)
           1)))
     ((= 2 (grid-dimension grid))
      (let ((y-min (cell-y-min cell)))
        (coordinate
         (- (* (- (x-coordinate x) x-min)
               (rectangular-2d-cell-scaling-factor cell 0))
            1)
         (- (* (- (y-coordinate x) y-min)
               (rectangular-2d-cell-scaling-factor cell 1))
            1)))))))

(defun grid-dimension (grid)
  (cell-dimension (cdar (grid-cell-alist grid))))

(defgeneric cell-get-neighbor (cell grid normal)
  (:documentation "Find the neighboring cell in the direction of normal"))

(defmethod cell-get-neighbor-index ((cell cell-1d) grid normal)
  (x-coordinate (coordinate-add (coordinate (get-cell-index cell grid))
                                normal)))

(defmethod cell-get-neighbor-index ((cell cell-2d) grid normal)
  (let* ((number-of-cells-x (grid-number-of-cells-x grid))
         (number-of-cells-y (grid-number-of-cells-y grid))
         (coord
           (coordinate-add
            (apply #'coordinate
                   (rectangular-2d-grid-to-local-index
                    (get-cell-index cell grid)
                    number-of-cells-x))
            normal))
         (x-index (x-coordinate coord))
         (y-index (y-coordinate coord)))
    (assert (and (>= x-index 0) (< x-index number-of-cells-x)
                 (>= y-index 0) (< y-index number-of-cells-y))
            nil
            "Cell with indices ~a, ~a outside of the grid"
            x-index
            y-index)
    (rectangular-2d-grid-to-global-index x-index y-index number-of-cells-x)))

(defun grid-boundary-side (cell-index normal side grid)
  (if (setup-p grid)
      (grid-boundary-side cell-index normal side (setup-grid grid))
      (with-slots (number-of-cells-x number-of-cells-y)
          grid
        (let* ((x-y-index-list (rectangular-2d-grid-to-local-index
                                cell-index
                                number-of-cells-x))
               (x-index (nth 0 x-y-index-list))
               (y-index (when number-of-cells-y
                          (nth 1 x-y-index-list))))
          (cond
            ((equalp side 'west)
             (and (west-p normal) (= 0 x-index)))
            ((equalp side 'east)
             (and (east-p normal) (= (1- number-of-cells-x) x-index)))
            ((equalp side 'north)
             (and (north-p normal) (= (1- number-of-cells-y) y-index)))
            ((equalp side 'south)
             (and (south-p normal) (= 0 y-index)))
            (t nil))))))

;; (defun grid-boundary-cell-p (cell-index grid)
;;   (if (setup-p grid)
;;       (grid-boundary-cell-p cell-index (setup-grid grid))
;;       (with-slots (number-of-cells-x number-of-cells-y)
;;           grid
;;         (if (not number-of-cells-y)
;;             (or (= 0 cell-index) (= (1- number-of-cells-x) cell-index))
;;             (let* ((x-y-index-list (rectangular-2d-grid-to-local-index
;;                                     cell-index
;;                                     number-of-cells-x))
;;                    (x-index (nth 0 x-y-index-list))
;;                    (y-index (nth 1 x-y-index-list)))
;;               (or (= 0 x-index)
;;                   (= (1- number-of-cells-x) x-index)
;;                   (= 0 y-index)
;;                   (= (1- number-of-cells-y) y-index)))))))

(defun grid-find-extreme-value (grid direction func)
  (assert (or (equalp func #'min)
              (equalp func #'max))
          nil
          "Invalid function ~a passed to grid-find-extreme value"
          func)
  (let ((dim (grid-dimension grid))
        (cells (mapcar #'cdr (grid-cell-alist grid))))
    (cond ((= 1 dim)
           (assert (= 0 direction)
                   nil
                   "Attempt to find y-min or y-max in 1d-grid")
           (apply func (mapcar #'x-coordinate (mapcar (if
                                                       (equalp func #'min)
                                                       #'x-min
                                                       #'x-max)
                                                      cells))))
          ((= 2 dim)
           (apply
             func
             (mapcar
              (lambda (cell)
               (rectangular-2d-cell-find-extreme-value cell direction func))
              cells)))
          (t (error "Dimension not supported yet")))))

(defun grid-x-min (grid)
  (grid-find-extreme-value grid 0 #'min))

(defun grid-x-max (grid)
  (grid-find-extreme-value grid 0 #'max))

(defun grid-y-min (grid)
  (grid-find-extreme-value grid 1 #'min))

(defun grid-y-max (grid)
  (grid-find-extreme-value grid 1 #'max))

(defun cell-x-mid (cell)
  (average (cell-x-min cell) (cell-x-max cell)))

(defun cell-y-mid (cell)
  (average (cell-y-min cell) (cell-y-max cell)))

(defun normal-edge-direction (normal)
  "Given a normal vector of an edge of the 2d unit cell,
return the direction along which the edge is constant.
E.g. on the east edge, with normal (-1 0), return 0 because
x is constant along this edge"
  (if (= 1 (coordinate-dimension normal))
      0
      (cond
        ((and
          (not (= 0 (x-coordinate normal)))
          (= 0 (y-coordinate normal)))
         0)
        ((and
          (not (= 0 (y-coordinate normal)))
          (= 0 (x-coordinate normal)))
         1)
        (t (error "Cannot infer edge from normal")))))

(defun normal-edge-along-direction (normal)
  "Given a normal vector of an edge of the 2d unit cell,
return the direction along which the edge is constant.
E.g. on the east edge, with normal (-1 0), return 1 because it
it goes along the y-direction"
  (if (= 1 (coordinate-dimension normal))
      1
      (cond
        ((and
          (not (= 0 (x-coordinate normal)))
          (= 0 (y-coordinate normal)))
         1)
        ((and
          (not (= 0 (y-coordinate normal)))
          (= 0 (x-coordinate normal)))
         0)
        (t (error "Cannot infer edge from normal")))))
