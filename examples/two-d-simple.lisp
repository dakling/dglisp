(in-package :dglisp)

;; solve 
;; du/dx + du/dy = 1
;; RB:
;; u(x,0) = 1/2 x;
;; u(0,y) = 1/2 y;
;; -> solution: u = 1/2(x+y)

(defform boundary-conditions (normal cell &optional u-out)
  (cond
    ((east-p normal)
     u-out)
    ((west-p normal)
     (list
      (polynomial-insert-unit-coordinates
       cell
       (make-polynomial-from-list (list 0 0.5 0.5) 2))))
    ((north-p normal)
     u-out)
    ((south-p normal)
     (list
      (polynomial-insert-unit-coordinates
       cell
       (make-polynomial-from-list (list 0 0.5 0.5) 2))))
    (t (error "Not a boundary edge"))))

(defform two-d-simple-volume-form (u v grad-u grad-v setup)
  (-
   (+
    (*
     (nth 0 u)
     (+
      (nth 0 grad-v)
      (nth 1 grad-v)))
    v)))

(defform two-d-simple-inner-edge-flux (u-in u-out normal)
  (* 0.5
     (+ (nth 0 u-in) (nth 0 u-out))
     (+ (x-coordinate normal) (y-coordinate normal))))

(defform two-d-simple-boundary-edge-flux (u-out normal)
  (two-d-simple-inner-edge-flux u-out u-out normal))

(defform two-d-simple-inner-edge-form (u-in u-out v normal setup)
  (* v (two-d-simple-inner-edge-flux u-in u-out normal)))

(defform two-d-simple-boundary-edge-form (u-out v normal cell setup)
  (* v (two-d-simple-boundary-edge-flux
        (boundary-conditions normal cell u-out)
        normal)))

(defvar *two-d-simple-setup* (make-setup
                              :grid (make-rectangular-grid-automatically (list 4 4) (list 0 1 0 1))
                              :dg-degree 1
                              :variable-name-list (list "u")
                              :boundary-edge-form-list (list
                                                        #'two-d-simple-boundary-edge-form)
                              :inner-edge-form-list (list
                                                     #'two-d-simple-inner-edge-form)
                              :volume-form-list (list
                                                 #'two-d-simple-volume-form)))

(defvar *two-d-simple-sol* (solve *two-d-simple-setup*))

(write-fields-to-vtk *two-d-simple-sol* *two-d-simple-setup* 0.01 "two-d-simple")
