(in-package :dglisp)

;; TODO: improvements
;; - generalize code (?)
;; - defform: handle calls from other system
;; - multiple dimensions
;; - "user interface" -> not all terms in one form
;; - nonlinear equations
;; - ... ?

;;; solve y' + y = 0
;;; BC: y(0) = 1

(defconstant +alpha+ 0.5)

(defform simple-example-inner-edge-flux (u-in u-out normal setup)
  (*
   (+ (if (east-p normal)
          (+ (* (- 1 +alpha+) (nth 0 u-in)) (* +alpha+ (nth 0 u-out))) ;right edge -> u-in old, u-out new
          (+ (* +alpha+ (nth 0 u-in)) (* (- 1 +alpha+) (nth 0 u-out)))) ;left edge -> u-in new, u-out old
      (* (setup-get-penalty setup)
         (- (nth 0 u-in) (nth 0 u-out))))
   (x-coordinate normal)))

(defform simple-example-boundary-edge-flux (u-out normal setup)
  (simple-example-inner-edge-flux u-out u-out normal setup))

(defform simple-example-inner-edge-form (u-in u-out v normal setup)
  (* (simple-example-inner-edge-flux u-in u-out normal setup) v))

(defform simple-example-boundary-edge-form (u-out v normal cell setup)
  (let ((y-init 1.0))
   (* (if (west-p normal)
         (simple-example-boundary-edge-flux (list y-init) normal setup); left edge (dirichlet)
         (simple-example-boundary-edge-flux u-out normal setup)); right edge ("outflow")
     v)))

(defform simple-example-volume-form (u v grad-u grad-v setup)
  (+
   (* (nth 0 u) v)
   (- (* (nth 0 u) (nth 0 grad-v)))))

(defvar *simple-ode-setup* (make-setup
                            :grid (make-rectangular-grid-automatically (list 3) (list 0 2))
                            :dg-degree 2
                            :boundary-edge-form-list (list #'simple-example-boundary-edge-form)
                            :inner-edge-form-list (list #'simple-example-inner-edge-form)
                            :volume-form-list (list #'simple-example-volume-form)))

(defvar *simple-ode-sol* (solve *simple-ode-setup*))

(plot-sol *simple-ode-sol*
          *simple-ode-setup*
          "simple-ode"
          (list 0)
          (lambda (x) (exp (- x))))
