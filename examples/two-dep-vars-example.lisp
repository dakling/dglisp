(in-package :dglisp)

;; solve the equation system
;; y1' - y2 = 0
;; y2' + y1 = 0
;; which is equivalent to y'' + y = 0
;; BCS: y1(0) = 0; y2(0) = 1 -> yields y1(x) = sin(x), y2(x) = cos(x)

(defform two-var-ex-inner-edge-flux (u-in u-out normal setup depvar)
  (* (+ (if (east-p normal)
            (+ (* (- 1 +alpha+) (nth depvar u-in)) (* +alpha+ (nth depvar u-out))) ;right edge -> u-in old, u-out new
            (+ (* +alpha+ (nth depvar u-in)) (* (- 1 +alpha+) (nth depvar u-out)))) ;left edge -> u-in new, u-out old
        (* (setup-get-penalty setup)
           (- (nth depvar u-in) (nth depvar u-out))))
     (x-coordinate normal)))

(defform two-var-ex-boundary-edge-flux (u-out normal setup depvar)
  (two-var-ex-inner-edge-flux u-out u-out normal setup depvar))

(defform two-var-ex-inner-edge-form-0 (u-in u-out v normal setup)
  (* (two-var-ex-inner-edge-flux u-in u-out normal setup 0) v))

(defform two-var-ex-inner-edge-form-1 (u-in u-out v normal setup)
  (* (two-var-ex-inner-edge-flux u-in u-out normal setup 1) v))

(defform two-var-ex-boundary-edge-form (u-out v normal setup depvar)
  (* (if (west-p normal)
         (two-var-ex-boundary-edge-flux (list 0.0 1.0) normal setup depvar); left edge (dirichlet, y1(0) = 0, y2(0) 1)
         (two-var-ex-boundary-edge-flux u-out normal setup depvar)); right edge ("outflow")
     v))

(defform two-var-ex-boundary-edge-form-0 (u-out v normal cell-coordinate setup)
  (two-var-ex-boundary-edge-form u-out v normal setup 0))

(defform two-var-ex-boundary-edge-form-1 (u-out v normal cell-coordinate setup)
  (two-var-ex-boundary-edge-form u-out v normal setup 1))

(defform two-var-ex-volume-form-0 (u v grad-u grad-v setup)
  (+  (- (* 1 (nth 1 u) v))
      (- (* (nth 0 u) (nth 0 grad-v)))))

(defform two-var-ex-volume-form-1 (u v grad-u grad-v setup)
  (+  (* 1 (nth 0 u) v)
      (- (* (nth 1 u) (nth 0 grad-v)))))

(defvar *two-var-setup* (make-setup
                         :grid (make-rectangular-grid-automatically (list 10) (list 0 1))
                         :dg-degree 2
                         :boundary-edge-form-list (list
                                                   #'two-var-ex-boundary-edge-form-0
                                                   #'two-var-ex-boundary-edge-form-1)
                         :inner-edge-form-list (list
                                                #'two-var-ex-inner-edge-form-0
                                                #'two-var-ex-inner-edge-form-1)
                         :volume-form-list (list
                                            #'two-var-ex-volume-form-0
                                            #'two-var-ex-volume-form-1)))

(defvar *two-var-sol* (solve *two-var-setup*))

(plot-sol
 *two-var-sol*
 *two-var-setup*
 "two-var-example-0"
 (list 0)
 #'sin)

(plot-sol
 *two-var-sol*
 *two-var-setup*
 "two-var-example-1"
 (list 1)
 #'cos)

(plot-sol
 *two-var-sol*
 *two-var-setup*
 "two-var-example"
 (list 0 1)
 nil
 (list "u0" "u1"))
