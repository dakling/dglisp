(in-package :dglisp)

;; solve heat equation in 2D as a system:
;; div(u1) = 1
;; u1 + grad(u0) = 0;
;; RB:
;; u0 = 0;
;; u1*normal = 0;

;; u0 -> (nth u 0)
;; u1[0] -> (nth u 1)
;; u1[1] -> (nth u 2)
;; TODO: better notation for the above -> representation of u?

;; TODO: come up with sound boundary conditions
(defun boundary-conditions (normal &optional u-out)
  (cond
    ((east-p normal)
     (cons 1 (cdr u-out)))
    ((west-p normal)
     (cons 1 (cdr u-out)))
    ((north-p normal)
     (cons 1 (cdr u-out)))
    ((south-p normal)
     (cons 1 (cdr u-out)))
    (t (error "Not a boundary edge"))))

;; Equation 0
(defform two-d-volume-form-0 (u v grad-u grad-v setup)
  (- (+
      (* (nth 0 grad-v) (nth 1 u))
      (* (nth 1 grad-v) (nth 2 u))))) ; TODO: better notation for dot product

(defform two-d-inner-edge-flux-0 (u-in u-out normal)
  (let
      ((u1-in (subseq u-in 1))
       (u1-out (subseq u-out 1)))
    (* 0.5 (dot (mapcar #'+ u1-in u1-out) normal))))

(defform two-d-boundary-edge-flux-0 (u-out normal)
  (two-d-inner-edge-flux-0 u-out u-out normal))

(defform two-d-inner-edge-form-0 (u-in u-out v normal setup)
  (* v (two-d-inner-edge-flux-0 u-in u-out normal)))

(defform two-d-boundary-edge-form-0 (u-out v normal cell-coordinate setup)
  (* v (two-d-boundary-edge-flux-0
        (boundary-conditions normal u-out)
        normal)))

;; Equations 1 and 2
(defform two-d-volume-form-1 (u v grad-u grad-v setup)
  (-
   (* (nth 1 u) v)
   (* (nth 0 grad-v) (nth 0 u))))

(defform two-d-volume-form-2 (u v grad-u grad-v setup)
  (-
   (* (nth 2 u) v)
   (* (nth 1 grad-v) (nth 0 u))))

(defform two-d-inner-edge-flux-1-2 (u-in u-out normal component)
  (let
      ((u0-in (nth 0 u-in))
       (u0-out (nth 0 u-out)))
    (* 0.5 (+ u0-in u0-out) (nth component (coordinate-x-list normal)))))

(defform two-d-boundary-edge-flux-1-2 (u-out normal component)
  (two-d-inner-edge-flux-1-2 u-out u-out normal component))

(defform two-d-inner-edge-form-1 (u-in u-out v normal setup)
  (* v (two-d-inner-edge-flux-1-2 u-in u-out normal 0)))

(defform two-d-inner-edge-form-2 (u-in u-out v normal setup)
  (* v (two-d-inner-edge-flux-1-2 u-in u-out normal 1)))

(defform two-d-boundary-edge-form-1 (u-out v normal cell-coordinate setup)
  (* v (two-d-boundary-edge-flux-1-2
        (boundary-conditions normal u-out)
        normal
        0)))

(defform two-d-boundary-edge-form-2 (u-out v normal cell-coordinate setup)
  (* v (two-d-boundary-edge-flux-1-2
        (boundary-conditions normal u-out)
        normal
        1)))

(defvar *two-d-setup* (make-setup
                       :grid (make-rectangular-grid-automatically (list 2 2) (list 0 1 0 1))
                       :dg-degree 1
                       :variable-name-list (list "u0" "u1" "u2")
                       :boundary-edge-form-list (list
                                                 #'two-d-boundary-edge-form-0
                                                 #'two-d-boundary-edge-form-1
                                                 #'two-d-boundary-edge-form-2)
                       :inner-edge-form-list (list
                                              #'two-d-inner-edge-form-0
                                              #'two-d-inner-edge-form-1
                                              #'two-d-inner-edge-form-2)
                       :volume-form-list (list
                                          #'two-d-volume-form-0
                                          #'two-d-volume-form-1
                                          #'two-d-volume-form-2)))

(defvar *two-d-sol* (solve *two-d-setup*))

(write-fields-to-vtk *two-d-sol* *two-d-setup* 0.01 "two-d")

;; TODO representation of points, vectors, coordinates
