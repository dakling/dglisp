(in-package :dglisp)


(defun x-grid (sampling setup)
  (let ((x-min (setup-x-min setup))
        (x-max (setup-x-max setup))) 
    (mapcar (lambda (x)
              (coordinate (+ x-min
                             (* (- x-max x-min)
                                (/ x sampling)))))
            (range (1+ sampling)))))

(defun plot-var (x-axis
             solution-field-list
             setup
             &optional
             (filename "plot")
             (depvar 0)
             (label (format nil "Var ~a" depvar))
             (sampling 1000))
  (plt:plot
   x-axis
   (mapcar (continuous-sol (nth depvar solution-field-list)) (x-grid sampling setup))
   "--"
   :label label))

(defun plot-sol (solution-field-list
             setup
             &optional
               (filename "plot")
               (depvar-list (list 0))
               analytical-solution
               (label-list (make-list (length depvar-list)
                                      :initial-element "numerical solution"))
               (sampling 1000))

  (plt:clf)
  (when analytical-solution
    (plt:plot
     (mapcar #'x-coordinate (x-grid sampling setup))
     (mapcar analytical-solution (mapcar #'x-coordinate (x-grid sampling setup)))
     :label "analytical solution"))
  (mapcar
   (lambda (depvar label)
       (plot-var
        (mapcar #'x-coordinate (x-grid sampling setup))
        solution-field-list
        setup
        filename
        depvar
        label
        sampling))
   depvar-list
   label-list)
  (plt:legend)
  (plt:grid t)
  (plt:savefig filename))

(defun solution-field-to-tree (solution-field x-list y-list)
  (labels ((eval-field (x y)
             (funcall (continuous-sol solution-field)
                      (coordinate x y))))
    (mapcar
     (lambda (x)
       (mapcar
        (lambda (y)
          (list (eval-field x y)))
        y-list))
     x-list)))

(defun write-solution-fields-to-csv (solution-field-list &optional
                                                       (file #P"out.csv")
                                                       (sampling 5))
  (let*
      ((x-list (mapcar (lambda (x) (/ x sampling)) (range sampling))) ;TODO infer this from grid
       (y-list (mapcar (lambda (x) (/ x sampling)) (range sampling)))
       (x-y-grid (make-mesh x-list y-list)))
    (labels ((eval-field (x y field-index)
                         (funcall (continuous-sol (nth field-index solution-field-list))
                                  (coordinate x y)))
             (make-row (row-index)
               (alexandria:flatten
                (let* ((x-y (nth row-index x-y-grid))
                       (x (car x-y))
                       (y (cdr x-y)))
                  (list
                   x-y
                   (mapcar (lambda (field-index)
                             (eval-field
                              x
                              y
                              field-index))
                           (range (length solution-field-list))))))))
      (cl-csv:write-csv (mapcar #'make-row (range (length x-y-grid)))
                        :stream file))))

(defun write-fields-to-vtk (field-list setup &optional (sampling 0.1) (file "out"))
  (let* ((number-of-fields (length field-list))
         (x-min (setup-x-min setup))
         (x-max (setup-x-max setup))
         (y-min (setup-y-min setup))
         (y-max (setup-y-max setup))
         (x-list (coerce (np:arange x-min x-max sampling) 'list))
         (y-list (coerce (np:arange y-min y-max sampling) 'list))
         (z-list (coerce (np:arange 0 0.1 0.1) 'list))
         (u-list (mapcar
                  (lambda (field-index)
                    (solution-field-to-tree
                     (nth field-index field-list)
                     x-list
                     y-list))
                  (range number-of-fields))))
    (py4cl:remote-objects*
      (let ((x (np:arange x-min (+ sampling x-max) sampling))
            (y (np:arange y-min (+ sampling y-max) sampling))
            (z (np:arange 0 0.1 0.1)))
        (labels
            ((to-dict (field-index)
               (make-array (list (length x-list) (length y-list) 1)
                           :initial-contents
                           (nth field-index u-list))))
          (setq u-arg (make-hash-table))
          (mapcar
           (lambda (field-index)
             (setf (gethash (nth field-index (setup-variable-name-list setup))
                            u-arg)
                   (to-dict field-index)))
           (range number-of-fields))
          (vtk:gridtovtk file x y z u-arg))))))

