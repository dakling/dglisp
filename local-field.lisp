(in-package :dglisp)

(defstruct local-field
  cell
  solution-list)

(defun local-field-dimension (local-field)
  (cell-dimension (local-field-cell local-field)))

(defun local-field-continuous-sol (local-field)
  (let ((discrete-sol (local-field-solution-list local-field)))
    (lambda (x)
      (apply #'+
             (mapcar (lambda (n)
                         (eval-test-function n
                                             x
                                             (nth n discrete-sol)
                                             (local-field-dimension local-field)))
                     (range (length discrete-sol)))))))

(defun local-field-get-value (local-field x)
  (funcall (local-field-continuous-sol local-field) x))

;; (defun local-field-get-value-at-left-border (local-field)
;;   (local-field-get-value local-field -1))

;; (defun local-field-get-value-at-right-border (local-field)
;;   (local-field-get-value local-field 1))

(defun local-field-number-of-dofs-per-cell-and-depvar (local-field)
  (with-slots (solution-list)
      local-field
    (length solution-list)))
