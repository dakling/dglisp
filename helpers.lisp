(in-package :dglisp)

(defun range (number-of-steps)
  (labels
      ((iter (i)
         (unless (= i number-of-steps) (cons i (iter (1+ i))))))
    (coerce (iter 0) 'list)))

(defun average (&rest x)
  (/ (apply #'+ x) (length x)))

(defun unit-list (direction dimension)
  (mapcar
   (lambda (i) (if (= i direction)
              1
              0))
   (range dimension)))

(defun unit-vector (direction dimension)
  (let ((unit-list (unit-list direction dimension)))
    (magicl:from-list unit-list (list dimension 1) :type 'double-float)))

(defun zeros-vector (dimension)
  (magicl:zeros (list dimension 1)))

(defun eval-int (function min max)
  "Evaluate a given integral (i.e. [function]_min^max)"
  (- (funcall function max)
     (funcall function min)))

(defmacro einstein-sum (expr list)
  `(apply #'polynomial-add
          (mapcar
           ,expr
           ,list)))

(defun dot (list1 list2)
  "Calculate the dot product of two vectors represented as lists"
  (cond
  ;; handle scalars
    ((numberp list1)
     (dot (list list1) list2))
    ((numberp list2)
     (dot list1 (list list2)))
  ;; handle coordinates
    ((coordinate-p list1)
     (dot (coordinate-x-list list1) list2))
    ((coordinate-p list2)
     (dot (coordinate-x-list list2) list1))
    (t
     (assert (= (length list1) (length list2))
             nil
             "Length mismatch between ~a and ~a when attempting to calculate dot product."
             list1
             list2)
     (apply #'+
            (mapcar #'*
                    list1
                    list2)))))

(defun matrix-transpose (matrix)
  "Transpose a matrix represented as a tree, where we assume a list of
   rows, which in turn are lists of values. E.g. the matrix
   (1 2
    3 4)
   is represented as
   ((1 2) (3 4))"
  (assert (and (listp matrix)
               (every #'listp matrix))
          nil
          "The tree ~a cannot be understood as a matrix"
          matrix)
  (let ((number-of-rows (length matrix))
        (number-of-columns (length (car matrix))))
    (assert (every
             (lambda (row-length) (= number-of-columns row-length))
             (mapcar
              (lambda (row)
                (length (nth row matrix)))
              (range number-of-rows)))
            nil
            "Not all rows have the same length in matrix ~a"
            matrix)
    (mapcar
     (lambda (column)
       (mapcar
        (lambda (row)
          (nth column (nth row matrix)))
        (range number-of-rows)))
     (range number-of-columns))))

(defun matrix-product (matrix1 matrix2)
  "Calculate matrix product with matrices represented as trees, where we assume a list of
   rows, which in turn are lists of values. E.g. the matrix
   (1 2
    3 4)
   is represented as
   ((1 2) (3 4))"
  ;; TODO check dimensions
  (cond
    ((atom (car matrix1))               ;handle matrix1 being a vector
     (car (matrix-product (list matrix1) matrix2)))
    ((atom (car matrix2))               ;handle matrix2 being a vector
     (matrix-product matrix1 (list matrix2)))
    (t
     (let
         ((dot-operator
            (if (polynomial-p (caar matrix1))
                #'polynomial-dot
                #'dot)))
       (mapcar
        (lambda (row2)
          (mapcar
           (lambda (row1)
             (funcall dot-operator row1 row2))
           (matrix-transpose matrix2)))
        matrix1)))))

(defun mirror (list)
  "Mirror a vector represented as a list or a coordinate"
  (if (coordinate-p list)
      (apply #'coordinate (mirror (coordinate-x-list list)))
      (mapcar #'- list)))

(defmacro defform (name vars body)
  (translate-to-polynomial-expression
   `(defun ,name ,vars
      (progn ,body))))

(defun flatten-alist (alist)
  (cond
    ((null alist) nil)
    ((not (listp (cdar alist))) alist)
    (t (append (flatten-alist (car alist)) (flatten-alist (cdr alist))))))

(defun get-number-of-dofs-per-cell-and-depvar (dg-degree dimension)
  (cond ((= 1 dimension) (1+ dg-degree))
        ((= 2 dimension) (/ (+ (1+ dg-degree)
                               (expt (1+ dg-degree) 2))
                            2))
        (t (error "Dimension not supported yet"))))

(defun get-number-of-dofs-per-cell (dg-degree number-of-depvars dimension)
  (* number-of-depvars (get-number-of-dofs-per-cell-and-depvar dg-degree dimension)))

(defun remove-nth (n list)
  "Return a new list like list but with n-th element removed"
  (declare
    (type (integer 0) n)
    (type list list))
  (if (or (zerop n) (null list))
    (cdr list)
    (cons (car list) (remove-nth (1- n) (cdr list)))))

(defun triangular-number (n)
  (/ (+ n (expt n 2))
     2))

(defun triangular-root (x)
  (/ (- (sqrt (+ 1 (* x 8)))
        1)
     2))

(defun triangular-row (x)
  (floor (triangular-root x)))

(defun triangular-column (x)
  (let* ((row (triangular-row x))
         (leftmost-row-number (triangular-number row)))
    (- x leftmost-row-number)))

(defun make-mesh (list1 list2)
  "create a list of all possible pairs of list1 and list2"
  (flatten-alist
   (mapcar
    (lambda (x)
      (mapcar
       (lambda (y)
         (cons x y))
       list2))
    list1)))
