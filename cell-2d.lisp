(in-package :dglisp)

;; (defclass cell-2d (cell)
;;   ((node-list
;;    :initarg :node-list
;;    :accessor node-list)))

(defstruct (cell-2d (:constructor make-cell-2d (&rest node-list)))
  node-list)

;; (defmethod print-object ((obj cell-2d) stream)
;;   (print-unreadable-object (obj stream :type t)
;;     (with-slots (node-list)
;;         obj
;;       (format stream "~a;" node-list))))

(defun make-cell-2d-rectangle (&key x-min x-max y-min y-max)
  (let ((nodes
          (alexandria:flatten
           (mapcar
            (lambda (y)
                (mapcar
                 (lambda (x)
                     (coordinate x y))
                 (list x-min x-max)))
            (list y-min y-max)))))
    (apply #'make-cell-2d nodes)))

(defmethod cell-dimension ((cell cell-2d))
  2)

;; TODO generalize this to cells not created with #'make-cell-2d-rectangle
(defmethod cell-edge-list ((cell cell-2d))
  "Return a list of all outer edges of the cell"
  (with-slots (node-list) cell
    (list (list (nth 0 node-list) (nth 1 node-list))
          (list (nth 1 node-list) (nth 3 node-list))
          (list (nth 3 node-list) (nth 2 node-list))
          (list (nth 2 node-list) (nth 0 node-list)))))

;; (cell-edge-list (make-cell-2d-rectangle :x-min 0
;;                                         :x-max 1
;;                                         :y-min 3
;;                                         :y-max 4))
 
;; Taken from rosetta code
;; https://rosettacode.org/wiki/Ray-casting_algorithm#Common_Lisp
(defun ray-intersects-segment (point segment)
  (destructuring-bind (px . py) point
    (destructuring-bind ((ax . ay) (bx . by)) segment
      (when (< ay by)
        (rotatef ay by)
        (rotatef ax bx))
      (cond
       ;; point is above, below, or to the right of the rectangle
       ;; determined by segment; ray does not intesect the segment.
       ((or (> px (max ax bx)) (> py (max ay by)) (< py (min ay by)))
        nil)
       ;; point is to left of the rectangle; ray intersects segment
       ((< px (min ax bx))
        t)
       ;; point is within the rectangle...
       (t (let ((m-red (if (= ax bx) nil
                         (/ (- by ay) (- bx ax))))
                (m-blue (if (= px ax) nil
                          (/ (- py ay) (- px ax)))))
            (cond
             ((null m-blue) t)
             ((null m-red) nil)
             (t (>= m-blue m-red)))))))))

;; Adapted from rosetta code
;; https://rosettacode.org/wiki/Ray-casting_algorithm#Common_Lisp
(defun point-on-segment-p (point segment)
  (let ((tolerance 1e-5))
    (destructuring-bind (px . py) point
      (destructuring-bind ((ax . ay) (bx . by)) segment
        (when (< ay by)
          (rotatef ay by)
          (rotatef ax bx))
        ;; (when (or (= py ay) (= py by))
        ;;    (incf py epsilon))
        (cond
          ;; point is above, below, left or to the right of the rectangle
          ;; determined by segment; ray does not intesect the segment.
          ((or (< px (min ax bx)) (> px (max ax bx)) (> py (max ay by)) (< py (min ay by)))
           nil)
          ;; prevent division by zero
          ((= bx ax)
           (< (- px ax) tolerance))
          ;; prevent division by zero
          ((= by ay)
           (< (- py ay) tolerance))
          ;; point is within the rectangle...
          (t (< (- (/ (- px ax)
                      (- bx ax))
                   (/ (- py ay)
                      (- by ay)))
                tolerance)))))))

;; Adapted from rosetta code
;; https://rosettacode.org/wiki/Ray-casting_algorithm#Common_Lisp
(defun point-in-polygon (point polygon)
  (if (member t (mapcar (lambda (edge) ;TODO find out more ideomatic way to check if a list contains a true element
                          (point-on-segment-p point edge))
                        polygon))
      t
      (do ((in-p nil)) ((endp polygon) in-p)
        (when (ray-intersects-segment point (pop polygon))
          (setf in-p (not in-p))))))

(defmethod point-in-cell-p ((cell cell-2d) x)
  (point-in-polygon (cons (x-coordinate x) (y-coordinate x))
                    (mapcar
                     (lambda (edge)
                       (mapcar
                        (lambda (coordinate)
                          (cons (x-coordinate coordinate) (y-coordinate coordinate)))
                        edge))
                     (cell-edge-list cell))))

(defmethod unit-cell-normal-list ((cell cell-2d))
  (list (coordinate 1 0) (coordinate -1 0)
        (coordinate 0 1) (coordinate 0 -1)))

(defun assert-rect-2d-cell (cell)
  (assert (= 2 (cell-dimension cell))
          nil
          "This function should only be called for rectangular 2-d cells"))

(defun rectangular-2d-cell-find-extreme-value (cell direction func)
  (assert-rect-2d-cell cell)
  (let* ((node-list (cell-2d-node-list cell))
         (coords (mapcar (cond
                           ((= 0 direction) #'x-coordinate)
                           ((= 1 direction) #'y-coordinate))
                         node-list)))
    (apply func coords)))

(defun rectangular-2d-cell-xmin (cell)
  (rectangular-2d-cell-find-extreme-value cell 0 #'min))

(defun rectangular-2d-cell-xmax (cell)
 (rectangular-2d-cell-find-extreme-value cell 0 #'max))

(defun rectangular-2d-cell-ymin (cell)
 (rectangular-2d-cell-find-extreme-value cell 1 #'min))

(defun rectangular-2d-cell-ymax (cell)
 (rectangular-2d-cell-find-extreme-value cell 1 #'max))

(defun rectangular-2d-cell-scaling-factor (cell component)
  (let* ((delta-x (- (rectangular-2d-cell-xmax cell) (rectangular-2d-cell-xmin cell)))
         (delta-y (- (rectangular-2d-cell-ymax cell) (rectangular-2d-cell-ymin cell))))
    (nth component (list
                    (/ 2 delta-x)
                    (/ 2 delta-y)))))

(defun rectangular-2d-cell-transformation-matrix (cell)
  (list
   (list (rectangular-2d-cell-scaling-factor cell 0) 0)
   (list 0 (rectangular-2d-cell-scaling-factor cell 1))))

(defun rectangular-2d-cell-transformation-det (cell)
  (* (rectangular-2d-cell-scaling-factor cell 0)
     (rectangular-2d-cell-scaling-factor cell 1)))

(defmethod cell-transform-from-unit-cell ((cell cell-2d) expression)
  (/ expression (rectangular-2d-cell-transformation-det cell)))

(defmethod cell-transform-from-unit-edge ((cell cell-2d) normal expression)
  (let ((direction (normal-edge-along-direction normal)))
    (/ expression
       (nth
        direction
        (nth direction
         (rectangular-2d-cell-transformation-matrix cell))))))

(defmethod cell-transform-to-unit-cell ((cell cell-2d) expression)
  (matrix-product expression
                  (rectangular-2d-cell-transformation-matrix cell)))

(defmethod cell-delta ((cell cell-2d) direction)
  (/ 2 (rectangular-2d-cell-scaling-factor cell direction)))

(defmethod cell-min ((cell cell-2d) direction)
  (rectangular-2d-cell-find-extreme-value cell direction #'min))

(defmethod cell-x-min ((cell cell-2d))
  (rectangular-2d-cell-xmin cell))

(defmethod cell-x-max ((cell cell-2d))
  (rectangular-2d-cell-xmax cell))

(defmethod cell-y-min ((cell cell-2d))
  (rectangular-2d-cell-ymin cell))

(defmethod cell-y-max ((cell cell-2d))
  (rectangular-2d-cell-ymax cell))

(defmethod cell-coordinate ((cell cell-2d))
  (coordinate (cell-x-mid cell) (cell-y-mid cell)))

(defmethod polynomial-insert-unit-coordinates ((cell cell-2d) polynomial)
  (let ((x-min (cell-x-min cell))
        (y-min (cell-y-min cell))
        (delta-x (cell-delta cell 0))
        (delta-y (cell-delta cell 1)))
    (polynomial-insert-polynomial
     (polynomial-insert-polynomial
      polynomial
      1
      (make-polynomial-from-list
       (list (+ y-min (/ delta-y 2)) 0 (/ delta-y 2))
       2))
     0
     (make-polynomial-from-list
      (list (+ x-min (/ delta-x 2)) (/ delta-x 2) 0)
      2))))

;; (polynomial-simplify
;;  (polynomial-insert-unit-coordinates (get-cell 0
;;                                                (make-rectangular-grid-automatically
;;                                                 (list 1 1)
;;                                                 (list 0 2 0 2)))
;;                                      (make-polynomial-from-list
;;                                       (list 0 (/ 1 2) (/ 1 2))
;;                                       2)))
