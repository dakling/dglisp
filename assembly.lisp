(in-package :dglisp)

(defun u-edge-list (cell-index setup field-list normal)
  (let ((direction (normal-edge-direction normal)))
      (mapcar
       (lambda (depvar)
         (polynomial-partial-eval
          (einstein-sum
           (lambda (i)
             (test-function
              i
              (field-list-get-coefficient
               cell-index
               i
               depvar
               field-list)
              (field-list-dimension field-list)))
           (range (setup-number-of-dofs-per-cell-and-variable setup)))
          direction
          (get-coordinate normal direction)))
       (range (setup-number-of-dependent-variables setup)))))

(defun u-volume-list (cell-index setup field-list)
  (mapcar
   (lambda (depvar)
     (einstein-sum
      (lambda (i)
        (test-function
         i
         (field-list-get-coefficient
          cell-index
          i
          depvar
          field-list)
         (field-list-dimension field-list)))
      (range (setup-number-of-dofs-per-cell-and-variable setup))))
   (range (setup-number-of-dependent-variables setup))))

(defun grad-u-volume-list (cell-index setup field-list)
  (cell-transform-to-unit-cell
   (get-cell cell-index (setup-grid setup))
   (mapcar
    #'polynomial-grad
    (mapcar
     (lambda (depvar)
       (einstein-sum
        (lambda (i)
          (test-function
           i
           (field-list-get-coefficient
            cell-index
            i
            depvar
            field-list)
           (field-list-dimension field-list)))
        (range (setup-number-of-dofs-per-cell-and-variable setup))))
     (range (setup-number-of-dependent-variables setup))))))

(defun eval-edge-form (cell-index j setup field-list normal &optional (dep-var 0))
  (let ((boundary-edge-form-list (setup-boundary-edge-form-list setup))
        (inner-edge-form-list (setup-inner-edge-form-list setup))
        (dimension (field-list-dimension field-list))
        (cell (get-cell cell-index (setup-grid setup)))
        (direction (normal-edge-direction normal)))
    (cond ((setup-boundary-edge-p cell-index normal setup) ; boundary edge
           (cell-transform-from-unit-edge
            cell
            normal
            (polynomial-integrate-over-unit-edge
             (funcall (nth dep-var boundary-edge-form-list)
                      ;; u
                      (u-edge-list cell-index setup field-list normal)
                      ;; v
                      (polynomial-partial-eval
                       (trial-function j dimension)
                       direction
                       (get-coordinate normal direction))
                      normal
                      cell
                      setup)
             normal)))
          (t ; inner edge
           (cell-transform-from-unit-edge
            cell
            normal
            (polynomial-integrate-over-unit-edge
             (funcall (nth dep-var inner-edge-form-list)
                      ;; u-in
                      (u-edge-list cell-index setup field-list normal)
                      ;; u-out
                      (u-edge-list
                       (cell-get-neighbor-index cell (setup-grid setup) normal)
                       setup
                       field-list
                       (mirror normal))
                      ;; v
                      (polynomial-partial-eval
                       (trial-function j dimension)
                       direction
                       (get-coordinate normal direction))
                      normal
                      setup)
             normal))))))

(defun eval-volume-form (cell-index j setup field-list &optional (dep-var 0))
  (let ((volume-form-list (setup-volume-form-list setup))
        (grid (setup-grid setup))
        (dimension (field-list-dimension field-list)))
    (cell-transform-from-unit-cell
     (get-cell cell-index grid)
     (polynomial-integrate-over-unit-cell
      (funcall (nth dep-var volume-form-list)
               ;; u
               (u-volume-list cell-index setup field-list)
               ;; v
               (trial-function j dimension)
               ;; grad-u
               (grad-u-volume-list cell-index setup field-list)
               ;; grad-v
               (cell-transform-to-unit-cell
                (get-cell cell-index grid)
                (polynomial-grad
                 (trial-function j dimension)))
               ;; setup
               setup)))))

(defun assemble-implicit-discrete-equation (setup field-list)
  (lambda (cell-index j dep-var)
    (apply #'+
           (eval-volume-form cell-index j setup field-list dep-var)
           (mapcar (lambda (normal)
                     (eval-edge-form cell-index j setup field-list normal dep-var))
                   (unit-cell-normal-list (get-cell cell-index (setup-grid setup)))))))

(defun implicit-discrete-equation (cell-index j depvar setup field-list)
  (funcall (assemble-implicit-discrete-equation
            setup
            field-list)
           cell-index
           j
           depvar))

(defun get-rhs (i setup)
  (- (implicit-discrete-equation (global-index-to-cell-index i setup)
                                 (global-index-to-degree-index i setup)
                                 (global-index-to-depvar-index i setup)
                                 setup
                                 (make-zeros-field-list setup))))

(defun get-coeff (i j setup)
  (+ (implicit-discrete-equation (global-index-to-cell-index i setup)
                                 (global-index-to-degree-index i setup)
                                 (global-index-to-depvar-index i setup)
                                 setup
                                 (make-unit-field-list j setup))
     (get-rhs i setup)))

;; build up A_ij in system A_ij * y_j = b_i (to be complemented by bcs)
(defun assemble-matrix (setup)
  (let* ((size (setup-number-of-dofs setup))
         (A (magicl:zeros (list size size))))
    (mapcar #'(lambda (i)
                (mapcar #'(lambda (j) (setf (magicl:tref A i j)
                                       (get-coeff i j setup)))
                        (range size)))
            (range size))
    A))

;; build up b_i in system A_ij * y_j = b_i (to be complemented by bcs)
(defun assemble-affine (setup)
  (let ((b (magicl:zeros (list (setup-number-of-dofs setup) 1))))
    (mapcar #'(lambda (j) (setf (magicl:tref b j 0) (get-rhs j setup)))
            (range (setup-number-of-dofs setup)))
    b))
