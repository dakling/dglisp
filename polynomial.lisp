(in-package :dglisp)

(defstruct polynomial
  monomial-list)

;; more convenient constructor
(defun make-polynomial-from-list (lst &optional (dimension 1))
  (let ((indices (range (length lst))))
    (make-polynomial :monomial-list
                     (mapcar #'(lambda (coeff index)
                               (make-monomial :coefficient coeff
                                              :power-list (make-power-list-from-index
                                                           index
                                                           dimension)))
                     lst
                     indices))))

(defmethod print-object ((obj polynomial) stream)
      (print-unreadable-object (obj stream :type t)
        (format stream "~{~a ~^+ ~}" (polynomial-monomial-list obj))))

(defun polynomial-eval (poly &rest x)
  "Evaluate poly at x"
  (apply #'+
         (mapcar (lambda (monom)
                   (apply #'monomial-eval monom x))
                 (polynomial-monomial-list poly))))

(defun polynomial-partial-eval (poly direction value)
  "Insert a value into a multivariate polynomial, e.g. x*y(y=3)"
  (make-polynomial
   :monomial-list
   (mapcar (lambda (monom)
             (monomial-partial-eval monom direction value))
           (polynomial-monomial-list poly))))

(defun polynomial-lambda (poly)
  "Turn poly into a function"
  (lambda (&rest x)
    (apply #'polynomial-eval poly x)))

(defun polynomial-monomial (poly power)
  (labels ((go-through-monomial-list (monom-list)
             (cond ((null monom-list) nil)
                   ((equalp (monomial-power (car monom-list)) power) (car monom-list))
                   (t (go-through-monomial-list (cdr monom-list))))))
    (go-through-monomial-list (polynomial-monomial-list poly))))

(defun polynomial-add-monomial (monom poly)
  (let* ((monom-list (polynomial-monomial-list poly))
         (exponent-list (mapcar #'monomial-exponent monom-list))
         (exponent (monomial-exponent monom)))
    (make-polynomial
     :monomial-list (cons
                     (if (member exponent exponent-list)
                         (monomial-add monom (polynomial-monomial poly exponent))
                         monom)
                     (remove exponent
                             (polynomial-monomial-list poly)
                             :test (lambda (exp mon)
                                     (= (monomial-exponent mon) exp)))))))

(defun polynomial-scale (poly factor)
  (if (numberp poly)
      (polynomial-scale (make-polynomial-from-list (list poly) 1) factor)
      (make-polynomial
       :monomial-list
       (mapcar #'(lambda (monom)
                   (make-monomial :power-list (monomial-power-list monom)
                                  :coefficient (* factor (monomial-coefficient monom))))
               (polynomial-monomial-list poly)))))

(defun polynomial-simplify (poly)
  (make-polynomial
   :monomial-list
   (monomial-list-combine (polynomial-monomial-list poly))))

(defun polynomial-add (&rest polys)
  (make-polynomial
   :monomial-list
   (apply
    #'append
    (mapcar
     #'polynomial-monomial-list
     (mapcar
      (lambda (poly)
        (if (numberp poly)             ; turn numbers into polynomials
            (make-polynomial
             :monomial-list
             (list
              (make-monomial :power-list nil
                             :coefficient poly)))
            poly))
      polys)))))

(defun polynomial-change-sign (poly)
  (polynomial-scale poly -1))

(defun polynomial-subtract (&rest polys)
  (if (= (length polys) 1)
      (polynomial-change-sign (car polys))
      (apply #'polynomial-add
             (cons
              (car polys)
              (mapcar #'polynomial-change-sign (cdr polys))))))

(defun polynomial-multiply-2 (poly1 poly2)
  (cond ((and (numberp poly1) (numberp poly2))
         (* poly1 poly2))
        ((and (numberp poly1) (polynomial-p poly2))
         (polynomial-scale poly2 poly1))
        ((and (numberp poly2) (polynomial-p poly1))
         (polynomial-scale poly1 poly2))
        ((and (polynomial-p poly1) (polynomial-p poly2))
         (make-polynomial
          :monomial-list
          (alexandria:flatten
           (mapcar #'(lambda (mon1)
                       (mapcar
                        #'(lambda (mon2)
                            (monomial-multiply mon1 mon2))
                        (polynomial-monomial-list poly1)))
                   (polynomial-monomial-list poly2)))))
        (t (error "Invalid input to polynomial-multiply-2"))))

(defun polynomial-multiply (poly1 &rest polys)
  (cond ((null polys) poly1) ; if only one is left
        (t (apply #'polynomial-multiply
                  (polynomial-multiply-2
                   poly1
                   (car polys))
                  (cdr polys)))))

;; This could be implemented much more efficiently, but we (probably) won't need that
(defun polynomial-expt (poly exponent)
  "Take the exponential of a polynomial"
  (cond
    ((= 0 exponent) 1)
    (t (polynomial-simplify
        (polynomial-multiply poly (polynomial-expt poly (1- exponent)))))))

(defun polynomial-invert (poly)
  (cond ((numberp poly)
         (make-polynomial-from-list (list (/ poly))))
        ((= 1 (length (polynomial-monomial-list poly)))
         (make-polynomial :monomial-list
                          (monomial-invert (car (polynomial-monomial-list poly)))))
        (t (error "cannot invert polynomials with more than one term"))))

(defun polynomial-divide-2 (poly1 arg2)
  (cond ((and (numberp poly1)
              (numberp arg2))
         (make-polynomial-from-list (list (/ poly1 arg2))))
        ((and (polynomial-p poly1)
              (numberp arg2))
         (polynomial-scale poly1 (/ arg2)))
        ((and (polynomial-p poly1)
              (monomial-p arg2))
         (polynomial-multiply poly1 (make-polynomial
                                     :monomial-list
                                     (list (monomial-invert arg2)))))
        ((and (polynomial-p poly1)
              (polynomial-p arg2)
              (= 1 (length (polynomial-monomial-list arg2))))
         (polynomial-divide-2 poly1 (car (polynomial-monomial-list arg2))))
        (t (error "case ~A / ~A not handled by polynomial-divide-2" (type-of poly1) (type-of arg2)))))

(defun polynomial-divide (poly1 &rest polys)
  (cond ((null polys)
         (polynomial-invert poly1))
        ((= 1 (length polys))
         (polynomial-divide-2 poly1 (car polys)))
        (t (polynomial-divide (polynomial-divide-2 poly1 (car polys))
                              (cdr polys)))))

(defun polynomial-dot (poly1 poly2)
  (cond
    ((coordinate-p poly1)
     (polynomial-dot (coordinate-x-list poly1) poly2))
    ((coordinate-p poly2)
     (polynomial-dot (coordinate-x-list poly2) poly1))
    (t
     (apply #'polynomial-add
            (mapcar #'polynomial-multiply
                    poly1
                    poly2)))))

(defun polynomial-diff (polynomial direction)
  (make-polynomial
   :monomial-list
   (mapcar (lambda (monom)
             (monomial-diff direction monom))
           (polynomial-monomial-list polynomial))))

(defun polynomial-integrate (polynomial-expression direction &optional min max)
  (let* ((polynomial (eval polynomial-expression)) ;TODO check if simplifying actually makes things faster -> move to constructor?
         (integrated-polynomial
          (make-polynomial
           :monomial-list
           (mapcar (lambda (monom)
                     (monomial-integrate direction monom))
                   (polynomial-monomial-list polynomial)))))
    (if (and min max)
        (progn
         (eval-int
          (polynomial-lambda integrated-polynomial)
          min
          max))
        integrated-polynomial)))

(defun polynomial-dimension (poly)
  (apply #'max (mapcar #'monomial-dimension (polynomial-monomial-list poly))))

(defun polynomial-grad (poly)
  (let ((dimension (polynomial-dimension poly)))
    (mapcar (lambda (dim)
              (polynomial-diff poly dim))
            (range dimension))))

(defun polynomial-integrate-over-unit-cell (polynomial-expression)
  (let ((dimension (polynomial-dimension (eval polynomial-expression))))
    (cond
      ((= 1 dimension)
       (eval-int (polynomial-lambda (polynomial-integrate
                                     polynomial-expression
                                     0))
                 (coordinate -1) (coordinate 1)))
      ((= 2 dimension)
       (let ((func
               (polynomial-lambda
                (polynomial-integrate
                 (polynomial-integrate polynomial-expression
                                       0)
                 1))))
         (-
          (+ (funcall func (coordinate 1 1))
             (funcall func (coordinate -1 -1)))
          (+ (funcall func (coordinate -1 1))
             (funcall func (coordinate 1 -1)))))); TODO write this more nicely
      (t (error "Dimension not supported yet"))))) 

(defun polynomial-integrate-over-unit-edge (polynomial-expression normal)
  (cond
    ((or (numberp polynomial-expression)
         (= 1 (polynomial-dimension polynomial-expression)))
     (polynomial-eval polynomial-expression normal))
    ((= 2 (polynomial-dimension polynomial-expression))
     (let ((direction (normal-edge-along-direction normal)))
       (polynomial-integrate
        polynomial-expression
        direction
        (unit-coordinate direction 2 -1)
        (unit-coordinate direction 2 1))))))

;; TODO
(defun polynomial-degree (polynomial direction)
  (1-
   (length
    (polynomial-monomial-list
     (polynomial-simplify
      (polynomial-partial-eval
       polynomial
       direction
       1))))))

(defun monomial-insert-polynomial (monomial direction polynomial)
  "Insert a variable transformation given by polynomial into monomial"
  (with-slots (coefficient power-list)
      monomial
   (polynomial-multiply
    (make-polynomial
     :monomial-list
     (list
      (make-monomial
       :coefficient coefficient
       :power-list (power-list-remove power-list direction))))
    (polynomial-expt polynomial (monomial-degree monomial direction)))))

;; (polynomial-simplify
;;  (monomial-insert-polynomial
;;   (make-monomial
;;    :coefficient 2
;;    :power-list (make-power-list-from-pairs '((0 . 4) (1 . 2))))
;;   1
;;   (make-polynomial-from-list (list 1 0 2)
;;                              2)))

(defun polynomial-insert-polynomial (polynomial direction transformation-polynomial)
  "Insert a variable transformation given by transformation-polynomial into polynomial"
  (apply
   #'polynomial-add
   (mapcar
    (lambda (monom)
      (monomial-insert-polynomial monom direction transformation-polynomial))
    (polynomial-monomial-list polynomial))))

;; (polynomial-insert-polynomial
;;  (make-polynomial-from-list (list 1 2 2 4 5)
;;                             2)
;;  0
;;  (make-polynomial-from-list (list 1 0 2)
;;                             2))
