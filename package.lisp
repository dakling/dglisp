;;;; package.lisp

(defpackage #:dglisp
  (:use #:cl)
  (:export
   defform
   make-setup
   solve
   plot-sol))

(eval-when (:compile-toplevel :execute :load-toplevel)
  (py4cl:import-module "matplotlib.pyplot" :as "plt" :reload t)
  (py4cl:import-module "pyevtk.hl" :as "vtk" :reload t)
  (py4cl:import-module "numpy" :as "np" :reload t))
