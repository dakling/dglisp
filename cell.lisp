(in-package :dglisp)

(defclass cell ()
  ())

;; (defgeneric cell-get-size (cell)
;;   (:documentation "get size of a cell"))

(defgeneric cell-transform-to-unit-cell (cell expression)
  (:documentation "Transform expression from cell to unit cell"))

(defgeneric cell-transform-from-unit-cell (cell expression)
  (:documentation "Transform expression to cell from unit cell"))

(defgeneric cell-dimension (cell)
  (:documentation "Return the dimension of the cell"))

(defgeneric point-in-cell-p (cell x)
  (:documentation "Check if point with coordinates x lies in cell"))

;; TODO generalize to n-d
;; (defgeneric cell-get-value-at-left-border (cell &optional (dep-var 0))
;;   (:documentation "get value of solution at left cell border"))

;; (defgeneric cell-get-value-at-right-border (cell &optional (dep-var 0))
;;   (:documentation "get value of solution at right cell border"))

(defgeneric unit-cell-normal-list (cell)
  (:documentation "Return all normals of the unit cell. Note that in all unit cells,
the normal vector on each edge is also the coordinate of the midpoint of that edge.
We therefore use the normal vector to project functions and as the point where the
edge forms are evaluated"))

