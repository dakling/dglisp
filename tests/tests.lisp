;; (defpackage dglisp-test
;;   (:use
;;    :cl
;;    :dglisp
;;    :prove))

;; (in-package :dglisp-test)

(in-package :dglisp)

;; helpers
(assert (and
         (= 32 (dot (list 1 2 3)
                    (list 4 5 6)))
         (= 12 (dot 3 4))))

(assert (equal
         (matrix-transpose '((1 2 3) (4 5 6) (7 8 9)))
         '((1 4 7) (2 5 8) (3 6 9))))

(assert (equal
         '((14 18)
           (34 38))
         (matrix-product
          '((1 2)
            (3 4))
          '((6 2)
            (4 8)))))

;; coordinates
(assert
 (equalp
  (coordinate-add (coordinate 3 0 -2)
                  (coordinate 4 5 5))
  (coordinate 7 5 3)))

(assert
 (and
  (west-p (coordinate -1 0))
  (east-p (coordinate 1 0))
  (north-p (coordinate 0 1))
  (south-p (coordinate 0 -1))))


;; monomials
(assert (= 12 (monomial-eval (make-monomial :coefficient 1
                                            :power-list (make-power-list-from-pairs '((0 . 1) (1 . 1))))
                             6
                             2)))


;; polynomials
(assert (= 18 (polynomial-eval (make-polynomial-from-list (list 1 2 3) 2)
                               4
                               3)))

(assert (= 46 (polynomial-eval (polynomial-add (make-polynomial-from-list (list 1 2 3 3) 2)
                                               (make-polynomial-from-list (list 2 3 4) 2))
                               2
                               3)))

(assert (= 1 (polynomial-dimension (make-polynomial-from-list (list 1) 1))))

(assert (= 2 (polynomial-dimension (make-polynomial-from-list (list 1) 2))))

(assert (= 1 (polynomial-dimension (make-polynomial-from-list (list 1 2 3) 1))))

(assert (= 2 (polynomial-dimension (make-polynomial-from-list (list 1 2 3) 2))))

;; TODO -> polynomial equality
;; (equalp
;;  (polynomial-simplify (make-polynomial-from-list (list 64 17 4) 1))
;;  (polynomial-simplify (polynomial-partial-eval (make-polynomial-from-list (list 1 2 3 4 5 6)
;;                                                                           2)
;;                                                1
;;                                                3)))

;; cells
(assert (= 0.5 (cell-1d-get-size (make-cell-1d :x-min (coordinate 0.5)
                                               :x-max (coordinate 1)))))

(assert (and (point-in-cell-p
              (make-cell-2d-rectangle :x-min 0
                                      :x-max 1
                                      :y-min 3
                                      :y-max 4)
              (coordinate 0.5 3.5))
             (point-in-cell-p
              (make-cell-2d-rectangle :x-min 0
                                      :x-max 1
                                      :y-min 3
                                      :y-max 4)
              (coordinate 1 3))
             (point-in-cell-p
              (make-cell-2d-rectangle :x-min 0
                                      :x-max 1
                                      :y-min 3
                                      :y-max 4)
              (coordinate 0 4))
             (not (point-in-cell-p
                   (make-cell-2d-rectangle :x-min 0
                                           :x-max 1
                                           :y-min 3
                                           :y-max 4)
                   (coordinate -0.1 3.5)))
             (not (point-in-cell-p
                   (make-cell-2d-rectangle :x-min 0
                                           :x-max 1
                                           :y-min 3
                                           :y-max 4)
                   (coordinate 0.1 4.1)))))

;; grid
(let ((grid (make-rectangular-grid-automatically (list 5) (list 1 3))))
  (assert (= 2
             (get-cell-index
              (get-cell 2 grid)
              grid)))
  (assert (= 3
              (cell-get-neighbor-index
               (get-cell 2 grid)
               grid
               (coordinate 1)))))

(let ((grid (make-rectangular-grid-automatically (list 3 3) (list 1 3 0 1))))
  (assert
   (= 4
      (get-cell-index
       (get-cell 4 grid)
       grid)))
  (assert
   (= 7
      (cell-get-neighbor-index
       (get-cell 4 grid)
       grid
       (coordinate 0 1)))))

;; (assert
;;  (and
;;   (grid-boundary-cell-p 3 (make-rectangular-grid-automatically (list 4) (list 0 1)))
;;   (grid-boundary-cell-p 15 (make-rectangular-grid-automatically (list 4 4) (list 0 1 0 1)))))

(assert
 (grid-boundary-side 3
                     (coordinate 1)
                     'east
                     (make-rectangular-grid-automatically (list 4) (list 4 5))))

(assert
 (grid-boundary-side 15
                     (coordinate 0 1)
                     'north
                     (make-rectangular-grid-automatically (list 4 4) (list 4 5 -3 -2))))

(assert
 (let ((grid (make-rectangular-grid-automatically (list 5) (list 5 6))))
   (and
    (= 5 (grid-x-min grid))
    (= 6 (grid-x-max grid)))))

(assert
 (let ((grid (make-rectangular-grid-automatically (list 5 4) (list 5 6 3 8))))
   (and
    (= 5 (grid-x-min grid))
    (= 6 (grid-x-max grid))
    (= 3 (grid-y-min grid))
    (= 8 (grid-y-max grid)))))

;; TODO
;; (assert
;;  (equalp
;;   (list
;;    (make-polynomial-from-list (list 0) 2)
;;    (make-polynomial-from-list (list 0.5) 2))
;;   (cell-transform-to-unit-cell
;;    (get-cell
;;     0
;;     (make-rectangular-grid-automatically
;;      (list 1 1)
;;      (list 4 5 2 6)))
;;    (mapcar #'polynomial-simplify
;;            (polynomial-grad
;;             (make-polynomial-from-list (list 0 0 1) 2))))))

;; local fields
(assert (= 5
           (local-field-get-value
            (make-local-field :cell (make-cell-1d :x-min (coordinate 1)
                                                  :x-max (coordinate 5))
                              :solution-list (list 1 2))
            2)))

