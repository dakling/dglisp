(in-package :dglisp)

(defstruct setup
  dg-degree
  (penalty 0)
  boundary-edge-form-list
  inner-edge-form-list
  volume-form-list
  grid
  variable-name-list)

(defun setup-number-of-dependent-variables (setup)
  (length (setup-volume-form-list setup)))

(defun setup-dimension (setup)
  (grid-dimension (setup-grid setup)))

(defun setup-number-of-cells (setup)
  (grid-number-of-cells (setup-grid setup)))

(defun setup-number-of-dofs-per-cell-and-variable (setup)
  (get-number-of-dofs-per-cell-and-depvar (setup-dg-degree setup)
                                          (setup-dimension setup)))

(defun setup-number-of-dofs-per-cell (setup)
  (get-number-of-dofs-per-cell (setup-dg-degree setup)
                               (setup-number-of-dependent-variables setup)
                               (setup-dimension setup)))

(defun setup-number-of-dofs (setup)
  (* (setup-number-of-cells setup)
     (setup-number-of-dofs-per-cell setup)))

;; TODO eventually make this a cell property
(defun setup-length-scale (setup)
  1)                           ;TODO

;; TODO eventually make this a cell property
(defun setup-get-penalty (setup)
  (let* ((dg-degree (setup-dg-degree setup))
         (penalty-basis (expt (1+ dg-degree) 2))
         (number-of-cells (setup-number-of-cells setup))
         (penalty (setup-penalty setup))
         (delta (setup-length-scale setup)))
    (* penalty penalty-basis delta)))

(defun global-index-to-cell-index (global-index setup)
  (floor global-index (* (setup-number-of-dofs-per-cell setup))))

(defun global-index-to-degree-index (global-index setup)
  (mod global-index (setup-number-of-dofs-per-cell-and-variable setup)))

(defun global-index-to-depvar-index (global-index setup)
  (floor (mod global-index (setup-number-of-dofs-per-cell setup))
         (setup-number-of-dofs-per-cell-and-variable setup)))

(defun setup-x-min (setup)
  (grid-x-min (setup-grid setup)))

(defun setup-x-max (setup)
  (grid-x-max (setup-grid setup)))

(defun setup-y-min (setup)
  (grid-y-min (setup-grid setup)))

(defun setup-y-max (setup)
  (grid-y-max (setup-grid setup)))

(defun setup-boundaries (setup)
  (cond ((= 1 (setup-dimension setup))
         '(west east))
        ((= 2 (setup-dimension setup))
         '(east west south north))))

(defun setup-boundary-edge-p (cell-index normal setup)
  (member
   t
   (mapcar
    (lambda (side)
      (grid-boundary-side cell-index normal side setup))
    (setup-boundaries setup))))
