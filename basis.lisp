(in-package :dglisp)

(defun test-function (n &optional (coeff 1) (dimension 1))
  (polynomial-scale
   (make-polynomial-from-list
    (unit-list n (1+ n))
    dimension)
   coeff))

(defun trial-function (n &optional (dimension 1))
  (test-function n 1 dimension))

(defun test-function-lambda (n &optional (coeff 1) (dimension 1))
  (polynomial-lambda (test-function n coeff dimension)))

(defun eval-test-function (n x &optional (coeff 1) (dimension 1))
  (polynomial-eval (test-function n coeff dimension) x))

(defun eval-trial-function (j x &optional (dimension 1))
  (polynomial-eval (trial-function j dimension) x))

(defun continuous-sol (field)
  (lambda (x)
    (funcall (local-field-continuous-sol (get-local-field-x field x))
             (to-local-coordinate x (field-grid field)))))

(defun translate-to-polynomial-expression (expr)
  (subst 'polynomial-add '+
         (subst 'polynomial-subtract '-
                (subst 'polynomial-multiply '*
                       (subst 'polynomial-dot 'dot expr)))))
