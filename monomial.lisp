(in-package :dglisp)

(defstruct monomial
  power-list
  (coefficient 1))

(defmethod print-object ((obj monomial) stream)
  (format stream
          "~f~{~a~}"
          (monomial-coefficient obj)
          (monomial-power-list obj)))

(defun monomial-lambda (monom)
  (lambda (&rest coord)
    (let ((x (if (and (= 1 (length coord)) (coordinate-p (car coord)))
                 (coordinate-x-list (car coord))
                 coord))
          (monom-power-list (monomial-power-list monom)))
      (assert (= (length monom-power-list) (length x))
              nil
              "Wrong number of arguments supplied to monomial")
      (* (monomial-coefficient monom)
         (power-list-eval
          (monomial-power-list monom)
          x)))))

(defun monomial-eval (monom &rest x)
  (apply (monomial-lambda monom) x))

(defun monomial-partial-eval (monom direction value)
  "Insert a value into a multivariate monomial, e.g. x*y(y=3)"
  (with-slots (power-list coefficient)
      monom
   (make-monomial
    :coefficient (* coefficient
                    (power-eval
                     (power-list-get-power power-list direction)
                     value))
    :power-list (power-list-replace
                 power-list
                 (make-power :direction direction
                             :exponent 0)))))

(defun monomial-diff (direction monom)
  (with-slots
        (power-list coefficient)
      monom
    (let ((exponent (power-list-get-exponent power-list direction)))
      (if (= 0 exponent)
          (make-monomial
           :coefficient 0
           :power-list power-list)
          (make-monomial
           :coefficient (* coefficient exponent)
           :power-list (power-list-replace power-list
                                           (make-power-from-pair
                                            (cons direction (1- exponent)))))))))

;; (monomial-diff (make-monomial
;;                 :power-list (make-power-list-from-pairs '((0 . 2) (1 . 4) (2 . 0)))
;;                 :coefficient 1)
;;                2)

(defun monomial-integrate (direction monom)
  (with-slots
        (power-list coefficient)
      monom
    (let ((exponent (power-list-get-exponent power-list direction)))
      (assert (>= exponent 0)
              nil
              "Supplied monomial has negative exponents, which does not make sense here.") ; TODO handle this case if necessary
      (make-monomial
       :coefficient (/ coefficient (1+ exponent))
       :power-list (power-list-replace power-list
                                       (make-power-from-pair
                                        (cons direction (1+ exponent))))))))

;; (monomial-integrate (monomial-diff
;;                      (make-monomial
;;                       :power-list (make-power-list-from-pairs '((0 . 0) (1 . 4)))
;;                       :coefficient 1)
;;                      0)
;;                     0)

(defun monomial-multiply (&rest monoms)
  (let ((coeffs (mapcar #'monomial-coefficient monoms))
        (power-lists (mapcar #'monomial-power-list monoms)))
    (make-monomial :power-list (power-list-combine (alexandria:flatten power-lists))
                   :coefficient (apply #'* coeffs))))

(defun monomial-invert (monom)
  (with-slots (power-list coefficient) monom
   (make-monomial :power-list (power-list-invert power-list)
                  :coefficient (/ coefficient))))

(defun monomial-add (monom1 monom2)
  (let ((coeff1 (monomial-coefficient monom1))
        (coeff2 (monomial-coefficient monom2))
        (powers1 (power-list-sort (monomial-power-list monom1)))
        (powers2 (power-list-sort (monomial-power-list monom2))))
    (if (equalp powers1 powers2)
        (make-monomial :power-list powers1
                       :coefficient (+ coeff1 coeff2))
        (make-polynomial :monomial-list
                         (list monom1 monom2)))))

;; (monomial-add (make-monomial :power-list (make-power-list-from-pairs '((0 . 1) (1 . 1))))
;;               (make-monomial :power-list (make-power-list-from-pairs '((0 . 1) (1 . 1)))
;;                              :coefficient 2))

(defun monomial-subtract (monom1 monom2)
  (let* ((coeff2 (monomial-coefficient monom2))
         (powers2 (power-list-sort (monomial-power-list monom2)))
         (-monom2 (make-monomial :coefficient (- coeff2)
                                 :power-list powers2)))
    (monomial-add monom1 -monom2)))

(defun monomial-list-combine (monomial-list)
  (labels ((iter (base-monom acc remaining)
             (cond
               ((null remaining) (cons base-monom acc))
               ((equalp (monomial-power-list base-monom) (monomial-power-list (car remaining)))
                (iter
                  (monomial-add base-monom (car remaining))
                  acc
                  (cdr remaining)))
               (t (iter
                    base-monom
                    (cons (car remaining) acc)
                    (cdr remaining)))))
           (combine-nth (n monoms)
             (iter (nth n monoms) nil (remove-nth n monoms)))
           (iter-2 (n monoms)
             (if (= (length monoms) n)
                 monoms
                 (iter-2 (1+ n) (combine-nth n monoms)))))
    (iter-2 0 monomial-list)))

(defun monomials-add (&rest monoms)
  (make-polynomial
   :monomial-list (monomial-list-combine monoms)))

(defun monomial-dimension (monom)
  (length (monomial-power-list monom)))

(defun monomial-degree (monom direction)
  (power-list-get-exponent (monomial-power-list monom)
                           direction))
