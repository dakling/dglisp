(in-package :dglisp)

;; an expression x_i^b, where direction stands for i, and exponent for b

(defstruct power
  (direction 0)                         ;0 for x, 1 for y,...
  exponent)

(defmethod print-object ((obj power) stream)
        (format stream
                "~a\^~a"
                (direction-to-letter (power-direction obj))
                (power-exponent obj)))

;; TODO write this nicer
(defun letter-to-direction (letter)
  "Allow simple notation, e.g. x,y,z instead of x_0, x_1, x_2."
  (ecase letter
    (x 0)
    (y 1)
    (z 2)))

(defun direction-to-letter (direction)
  "Allow simple notation, e.g. x,y,z instead of x_0, x_1, x_2."
  (case direction
    (0 #\x)
    (1 #\y)
    (2 #\z)
    (t direction)))

(defun make-power-from-pair (pair)
  "More convenient constructor"
  (make-power :direction (car pair)
              :exponent (cdr pair)))

(defun make-power-from-expt (expt-expr)
  "Allow creating power struct from quoted expt-expr, e.g. '(expt x 3)"
  (let ((direction (letter-to-direction (cadr expt-expr)))
        (exponent (caddr expt-expr)))
    (make-power-from-pair (cons direction exponent))))

(defun power-lambda (power)
  "Turn power into a callable function"
  (lambda (x)
    (expt x (if power
                (power-exponent power)
                0))))

(defun power-eval (power x)
  "Evaluate power at x"
  (funcall (power-lambda power)
           (if (and (coordinate-p x) (= 1 (coordinate-dimension x)))
               (x-coordinate x)
               x)))

(defun power-list-get-power (power-list direction)
  (cond
    ((null power-list) nil)
    ((= direction (power-direction (car power-list))) (car power-list))
    (t (power-list-get-power (cdr power-list) direction))))

(defun power-list-get-exponent (power-list direction)
  (let ((power (power-list-get-power power-list direction)))
   (if power
       (power-exponent power)
       0)))

(defun power-list-eval (power-list x-list)
  (apply #'*
         (mapcar
          (lambda (n)
            (power-eval (power-list-get-power power-list n) (nth n x-list)))
          (range (length x-list)))))

(defun powers-multiply (power1 power2)
  (assert (= (power-direction power1) (power-direction power2))
          nil
          "Product of ~a and ~a is not itself a power."
          power1
          power2)
  (make-power :direction (power-direction power1)
              :exponent (+ (power-exponent power1)
                           (power-exponent power2))))

(defun power-invert (power)
  (with-slots (direction exponent) power
    (make-power :direction direction
                :exponent (- exponent))))

(defun power-list-invert (power-list)
  (mapcar #'power-invert power-list))

(defun power-list-sort (power-list)
  (sort power-list (lambda (x y) (< (power-direction x) (power-direction y)))))

(defun power-list-combine (power-list)
  (labels ((iter (base-power acc remaining)
             (cond
               ((null remaining) (cons base-power acc))
               ((= (power-direction base-power) (power-direction (car remaining)))
                (iter
                  (powers-multiply base-power (car remaining))
                  acc
                  (cdr remaining)))
               (t (iter
                    base-power
                    (cons (car remaining) acc)
                    (cdr remaining)))))
           (combine-nth (n powers)
             (iter (nth n powers) nil (remove-nth n powers)))
           (iter-2 (n powers)
             (if (= (length powers) n)
                 powers
                 (iter-2 (1+ n) (combine-nth n powers)))))
    (iter-2 0 power-list)))

(defun make-power-list-from-pairs (pair-list)
  (power-list-combine
   (mapcar #'make-power-from-pair pair-list)))

(defun make-power-list-from-expts (expt-list)
  (power-list-combine
   (mapcar #'make-power-from-expt expt-list)))

;; In 1d: 1, x, x^2, x^3, ...
;; In 2d: 1, x, y, x^2, x*y, y^2, x^3, x^2*y, x*y^2, y^3, ...
(defun make-power-list-from-index (index dimension)
  (cond ((= 1 dimension)
         (list (make-power :direction 0
                           :exponent index)))
        ((= 2 dimension)
         (list (make-power :direction 0
                           :exponent (- (triangular-row index) (triangular-column index)))
               (make-power :direction 1
                           :exponent (triangular-column index))))
        (t (error "Dimension ~a not supported yet" dimension))))

(defun power-list-replace (power-list new-power)
  (let ((direction (power-direction new-power)))
    (if (= direction (power-direction (car power-list)))
        (cons new-power (cdr power-list))
        (cons (car power-list) (power-list-replace (cdr power-list) new-power)))))

(defun power-list-remove (power-list direction)
  (labels ((iter (pows)
             (if (= direction (power-direction (car pows)))
                 (cdr pows)
                 (cons (car pows) (iter (cdr pows))))))
    (iter (power-list-combine power-list))))
